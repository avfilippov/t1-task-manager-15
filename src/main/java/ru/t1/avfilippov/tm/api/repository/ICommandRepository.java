package ru.t1.avfilippov.tm.api.repository;

import ru.t1.avfilippov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
