package ru.t1.avfilippov.tm.repository;

import ru.t1.avfilippov.tm.api.repository.ICommandRepository;
import ru.t1.avfilippov.tm.constant.ArgumentConst;
import ru.t1.avfilippov.tm.constant.CommandConst;
import ru.t1.avfilippov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO,
            "show system info"
    );

    private static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT,
            "show developer's info"
    );

    private static final Command VERSION = new Command(
            CommandConst.VERSION, ArgumentConst.VERSION,
            "show application version"
    );

    private static final Command HELP = new Command(
            CommandConst.HELP, ArgumentConst.HELP,
            "show command list"
    );

    private static final Command EXIT = new Command(
            CommandConst.EXIT, null,
            "close application"
    );

    private static final Command PROJECT_CREATE = new Command(
            CommandConst.PROJECT_CREATE, null,
            "create new project"
    );

    private static final Command PROJECT_LIST = new Command(
            CommandConst.PROJECT_LIST, null,
            "display all projects"
    );

    private static final Command PROJECT_CLEAR = new Command(
            CommandConst.PROJECT_CLEAR, null,
            "remove all projects"
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            CommandConst.PROJECT_SHOW_BY_ID, null,
            "show project by id"
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            CommandConst.PROJECT_SHOW_BY_INDEX, null,
            "show project by index"
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            CommandConst.PROJECT_UPDATE_BY_ID, null,
            "update project by id"
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            CommandConst.PROJECT_UPDATE_BY_INDEX, null,
            "update project by index"
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            CommandConst.PROJECT_REMOVE_BY_ID, null,
            "remove project by id"
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            CommandConst.PROJECT_REMOVE_BY_INDEX, null,
            "remove project by index"
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            CommandConst.PROJECT_START_BY_ID, null,
            "start project by id"
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(
            CommandConst.PROJECT_START_BY_INDEX, null,
            "start project by index"
    );

    private static final Command PROJECT_COMPLETE_BY_ID = new Command(
            CommandConst.PROJECT_COMPLETE_BY_ID, null,
            "complete project by id"
    );

    private static final Command PROJECT_COMPLETE_BY_INDEX = new Command(
            CommandConst.PROJECT_COMPLETE_BY_INDEX, null,
            "complete project by index"
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            CommandConst.PROJECT_CHANGE_STATUS_BY_ID, null,
            "change project status by id"
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            CommandConst.PROJECT_CHANGE_STATUS_BY_INDEX, null,
            "change project status by index"
    );

    private static final Command TASK_CREATE = new Command(
            CommandConst.TASK_CREATE, null,
            "create new task"
    );

    private static final Command TASK_LIST = new Command(
            CommandConst.TASK_LIST, null,
            "display all tasks"
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            CommandConst.TASK_SHOW_BY_ID, null,
            "show task by id"
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            CommandConst.TASK_SHOW_BY_INDEX, null,
            "show task by index"
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            CommandConst.TASK_UPDATE_BY_ID, null,
            "update task by id"
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            CommandConst.TASK_UPDATE_BY_INDEX, null,
            "update task by index"
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            CommandConst.TASK_REMOVE_BY_ID, null,
            "remove task by id"
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            CommandConst.TASK_REMOVE_BY_INDEX, null,
            "remove task by index"
    );

    private static final Command TASK_CLEAR = new Command(
            CommandConst.TASK_CLEAR, null,
            "remove all tasks"
    );

    private static final Command TASK_START_BY_ID = new Command(
            CommandConst.TASK_START_BY_ID, null,
            "start task by id"
    );

    private static final Command TASK_START_BY_INDEX = new Command(
            CommandConst.TASK_START_BY_INDEX, null,
            "start task by index"
    );

    private static final Command TASK_COMPLETE_BY_ID = new Command(
            CommandConst.TASK_COMPLETE_BY_ID, null,
            "complete task by id"
    );

    private static final Command TASK_COMPLETE_BY_INDEX = new Command(
            CommandConst.TASK_COMPLETE_BY_INDEX, null,
            "complete task by index"
    );

    private static final Command TASK_CHANGE_STATUS_BY_ID = new Command(
            CommandConst.TASK_CHANGE_STATUS_BY_ID, null,
            "change task status by id"
    );

    private static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            CommandConst.TASK_CHANGE_STATUS_BY_INDEX, null,
            "change task status by index"
    );

    private static final Command TASK_BIND_TO_PROJECT = new Command(
            CommandConst.TASK_BIND_TO_PROJECT, null,
            "bind task to project"
    );

    private static final Command TASK_UNBIND_FROM_PROJECT = new Command(
            CommandConst.TASK_UNBIND_FROM_PROJECT, null,
            "unbind task from project"
    );

    private static final Command TASK_SHOW_BY_PROJECT_ID = new Command(
            CommandConst.TASK_SHOW_BY_PROJECT_ID, null,
            "show task list by project id"
    );

    public static final Command[] COMMANDS = new Command[]{
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX, PROJECT_UPDATE_BY_ID,
            PROJECT_UPDATE_BY_INDEX, PROJECT_REMOVE_BY_ID,
            PROJECT_REMOVE_BY_INDEX, PROJECT_START_BY_ID, PROJECT_START_BY_INDEX,
            PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_INDEX,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX,

            TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT, TASK_SHOW_BY_PROJECT_ID,

            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX, TASK_UPDATE_BY_ID,
            TASK_UPDATE_BY_INDEX, TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_INDEX, TASK_COMPLETE_BY_ID,
            TASK_COMPLETE_BY_INDEX, TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX,

            ABOUT, INFO, VERSION, HELP, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
