package ru.t1.avfilippov.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeStatusTaskById();

    void changeStatusTaskByIndex();

}
