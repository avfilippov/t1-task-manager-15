package ru.t1.avfilippov.tm;

import ru.t1.avfilippov.tm.component.Bootstrap;


public final class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
